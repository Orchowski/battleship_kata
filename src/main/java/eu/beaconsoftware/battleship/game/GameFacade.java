package eu.beaconsoftware.battleship.game;

import eu.beaconsoftware.battleship.game.representation.BoardRepresentation;

import java.util.Optional;

public interface GameFacade {
    BoardRepresentation startNewGame();

    BoardRepresentation hit(String location);
}
