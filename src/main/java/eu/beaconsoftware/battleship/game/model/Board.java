package eu.beaconsoftware.battleship.game.model;

import eu.beaconsoftware.battleship.game.entity.HitResult;
import eu.beaconsoftware.battleship.game.entity.Ship;
import eu.beaconsoftware.battleship.game.entity.Slot;
import eu.beaconsoftware.battleship.game.representation.BoardRepresentation;

import java.util.*;
import java.util.stream.IntStream;

import static eu.beaconsoftware.battleship.helper.StringHelper.asciiToString;

public class Board {
    private final static int MIN_SIZE = 5;
    private final static int MAX_SIZE = 26;
    private final TreeMap<String, List<Slot>> board = new TreeMap<>();
    private final ShipArrangementService arrangementService;

    public Board(final int size, final ShipArrangementService arrangementService) {
        if (size < MIN_SIZE || size > MAX_SIZE) {
            throw new IllegalArgumentException(
                    String.format("Size have to be between %d and %d", MIN_SIZE, MAX_SIZE)
            );
        }
        this.arrangementService = arrangementService;
        initializeBoard(size);
    }


    public HitResult hit(final Slot slotToHit) {
        final var boardSlot = this.board.get(slotToHit.column()).get(slotToHit.row() - 1);
        if (boardSlot.hasBeenHit()) {
            return HitResult.ALREADY_HIT;
        }
        this.board.get(boardSlot.column()).set(boardSlot.row() - 1, boardSlot.hit());

        return boardSlot.isEmpty() ? HitResult.MISS : HitResult.HIT;
    }

    public BoardRepresentation representation(){
        return BoardRepresentation.of(
                Collections.unmodifiableMap(this.board)
        );
    }

    public void place(final Ship ship) {
        final var slotsToTake = this.arrangementService.draw(board, ship);
        if (slotsToTake.stream().anyMatch(slot -> !slot.isEmpty())) {
            place(ship);
            return;
        }
        slotsToTake.forEach(
                slot -> takeSlot(slot, ship)
        );
    }

    private void takeSlot(final Slot slot, final Ship ship) {
        this.board.get(slot.column()).set(slot.row() - 1, slot.takeSlot(ship));
    }

    private void initializeBoard(final int size) {
        IntStream.range(65, 65 + size) //65 equals A in ASCII
                .forEachOrdered(columnId -> {
                            final var columnName = asciiToString(columnId);
                            this.board.put(
                                    columnName,
                                    new ArrayList<>(IntStream.range(1, size + 1).mapToObj(rowNumber ->
                                            new Slot(columnName, rowNumber)
                                    ).toList())
                            );
                        }
                );
    }
}