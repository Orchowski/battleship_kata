package eu.beaconsoftware.battleship.game.model;

import eu.beaconsoftware.battleship.game.entity.Ship;
import eu.beaconsoftware.battleship.game.entity.Slot;

import java.util.List;
import java.util.TreeMap;

import static eu.beaconsoftware.battleship.helper.StringHelper.asciiLetterToString;

public class DefaultShipArrangementService implements ShipArrangementService{
    public List<Slot> draw(final TreeMap<String, List<Slot>> board, final Ship ship){
        final var size = board.size();
        return switch (ship.orientation())  {
            case HORIZONTAL -> drawHorizontalArrangement(board,ship,size);
            case VERTICAL -> drawVerticalArrangement(board,ship,size);
        };
    }

    private List<Slot> drawHorizontalArrangement(final TreeMap<String, List<Slot>> board, final Ship ship, final int size){
        final var column = roll(size - 1);
        final var row = roll(size - 1 - ship.size());
        final var slots = board.get(asciiLetterToString(column)).subList(row, row + ship.size());
        return slots;
    }

    private List<Slot> drawVerticalArrangement(final TreeMap<String, List<Slot>> board, final Ship ship, final int size){
        final var column = roll(size - ship.size() - 1);
        final var row = roll(size - 1);
        final var slots = board.entrySet().stream().toList()
                .subList(column, column + ship.size()).stream().map(col -> col.getValue().get(row)).toList();
        return slots;
    }


    private int roll(final int max) {
        return 1 + (int) (Math.random() * max);
    }
}
