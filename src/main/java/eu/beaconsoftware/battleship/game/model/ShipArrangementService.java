package eu.beaconsoftware.battleship.game.model;

import eu.beaconsoftware.battleship.game.entity.Ship;
import eu.beaconsoftware.battleship.game.entity.ShipOrientation;
import eu.beaconsoftware.battleship.game.entity.Slot;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import static eu.beaconsoftware.battleship.helper.StringHelper.asciiLetterToString;

public interface ShipArrangementService {
    List<Slot> draw(final TreeMap<String, List<Slot>> board, final Ship ship);
}
