package eu.beaconsoftware.battleship.game.model;

import eu.beaconsoftware.battleship.game.GameFacade;
import eu.beaconsoftware.battleship.game.entity.Ship;
import eu.beaconsoftware.battleship.game.entity.ShipOrientation;
import eu.beaconsoftware.battleship.game.entity.ShipType;
import eu.beaconsoftware.battleship.game.entity.Slot;
import eu.beaconsoftware.battleship.game.representation.BoardRepresentation;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

@Service
public class Game implements GameFacade {
    public static final int DEFAULT_BOARD_SIZE = 10;
    private Optional<Board> board = Optional.empty();
    private List<Ship> ships = List.of();

    public Game(){
        this.arrangementService = new DefaultShipArrangementService();
    }

    public Game(ShipArrangementService arrangementService) {
        this.arrangementService = arrangementService;
    }

    private final ShipArrangementService arrangementService;

    @Override
    public BoardRepresentation startNewGame() {
        ships = List.of(
                new Ship(ShipType.BATTLESHIP, drawShipOrientation()),
                new Ship(ShipType.DESTROYER, drawShipOrientation()),
                new Ship(ShipType.DESTROYER, drawShipOrientation())
        );
        var board = new Board(DEFAULT_BOARD_SIZE, arrangementService);
        this.board = Optional.of(board);
        ships.forEach(board::place);

        var boardRepresentation = board.representation();
        boardRepresentation.ships().addAll(ships);
        return boardRepresentation;
    }

    @Override
    public BoardRepresentation hit(final String location) {
        Slot slotToHit = validateStateToHit(location);
        var board = this.board.get();
        board.hit(slotToHit);
        var boardRepresentation = board.representation();
        boardRepresentation.ships().addAll(ships);
        return boardRepresentation;
    }

    private Slot validateStateToHit(String location) {
        var slotToHit = Slot.fromText(location).orElseThrow(
                () -> new IllegalArgumentException("wrong location. Should be like A3, C10 without whitespaces"));
        board.orElseThrow(() -> new IllegalStateException("Game should be started first"));
        var board = this.board.get();
        var slotInRange = board
                .representation()
                .boardMap().values()
                .stream()
                .flatMap(Collection::stream)
                .anyMatch(slot -> slot.column().equals(slotToHit.column()) && slot.row() == slotToHit.row());
        if (!slotInRange) {
            throw new IllegalArgumentException("wrong location. Out of board size");
        }
        return slotToHit;
    }

    private ShipOrientation drawShipOrientation() {
        Function<Integer, Boolean> isEven = (number) -> number % 2 == 0;
        Supplier<Integer> roll = () -> (int) (Math.random() * 100);

        return isEven.apply(roll.get()) ? ShipOrientation.VERTICAL : ShipOrientation.HORIZONTAL;
    }
}
