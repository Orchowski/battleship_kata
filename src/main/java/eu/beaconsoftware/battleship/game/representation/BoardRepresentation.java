package eu.beaconsoftware.battleship.game.representation;

import eu.beaconsoftware.battleship.game.entity.Ship;
import eu.beaconsoftware.battleship.game.entity.Slot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public record BoardRepresentation(
        Map<String, List<Slot>> boardMap,
        List<Ship> ships) {

    public boolean allShipsSunk(){
        return this.ships.stream().allMatch(Ship::isSunk);
    }
    public static BoardRepresentation of(final Map<String, List<Slot>> boardMap) {
        return new BoardRepresentation(boardMap, new ArrayList<>());
    }
}