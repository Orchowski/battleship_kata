package eu.beaconsoftware.battleship.game.entity;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.regex.Pattern;

public record Slot(String column, int row, Optional<Ship> ship, boolean hasBeenHit) {
    public Slot(final String column, final int row) {
        this(column, row, Optional.empty(), false);
    }

    public Slot takeSlot(final Ship ship) {
        return new Slot(column, row, Optional.of(ship.takeSlot(this)), hasBeenHit);
    }

    public Slot hit() {
        return new Slot(column, row, ship.map(it->it.hit(this)), true);
    }

    public boolean isEmpty(){
        return this.ship.isEmpty();
    }

    @Override
    public String toString() {
        return column + row;
    }

    public static Optional<Slot> fromText(final String location){
        if (location == null || location.length() < 2){
            return Optional.empty();
        }
        var match = Pattern.matches("([A-Z]?)([0-9]+)", location);
        if (match){
            return Optional.of(
                    new Slot(location.substring(0,1),Integer.parseInt(location.substring(1)))
            );
        }
        return Optional.empty();
    }
}