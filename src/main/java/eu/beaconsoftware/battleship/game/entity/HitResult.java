package eu.beaconsoftware.battleship.game.entity;

public enum HitResult {
    MISS,
    HIT,
    ALREADY_HIT
}
