package eu.beaconsoftware.battleship.game.entity;

public enum ShipOrientation {
    HORIZONTAL,
    VERTICAL;
}
