package eu.beaconsoftware.battleship.game.entity;

public enum ShipType {
    BATTLESHIP(5),
    DESTROYER(4);

    final int size;

    ShipType(final Integer size) {
        this.size = size;
    }
}
