package eu.beaconsoftware.battleship.game.entity;

import java.util.ArrayList;
import java.util.List;

public class Ship {
    private final ShipType type;
    private final ShipOrientation orientation;
    private final List<Slot> slots = new ArrayList<>();

    public Ship(final ShipType type, final ShipOrientation orientation) {
        this.type = type;
        this.orientation = orientation;
    }

    public boolean isSunk(){
        return slots.stream().allMatch(Slot::hasBeenHit);
    }

    public int size() {
        return this.type.size;
    }

    public ShipOrientation orientation(){
        return orientation;
    }

    public Ship takeSlot(Slot slot) {
        slots.add(slot);
        return this;
    }

    public Ship hit(Slot slot) {
        slots.stream()
                .filter(s->s.column().equals(slot.column()) && s.row() == slot.row())
                .findFirst().ifPresent(
                        hitSlot -> slots.set(slots.indexOf(hitSlot),hitSlot.hit())
                );
        return this;
    }
}

