package eu.beaconsoftware.battleship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BattleshipApplication {

	public static void main(final String[] args) {
		SpringApplication.run(BattleshipApplication.class, args);
	}

}
