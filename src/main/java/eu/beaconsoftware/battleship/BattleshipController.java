package eu.beaconsoftware.battleship;

import eu.beaconsoftware.battleship.game.GameFacade;
import eu.beaconsoftware.battleship.game.representation.BoardRepresentation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;

@Controller
public class BattleshipController {
    private final GameFacade gameFacade;

    public BattleshipController(final GameFacade gameFacade) {
        this.gameFacade = gameFacade;
    }

    @GetMapping
    String startGame(final Model model){
        model.addAttribute("game", gameFacade.startNewGame());
        return "game";
    }
    @GetMapping("/hit")
    String hit(final Model model, @RequestParam String location){
        BoardRepresentation representation = gameFacade.hit(location);
        model.addAttribute("game", representation);
        return "game";
    }
}
