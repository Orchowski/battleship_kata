package eu.beaconsoftware.battleship.helper;

public class StringHelper {
    public static String asciiToString(final int code){
        return Character.toString((char) code);
    }
    public static String asciiLetterToString(final int code){
        return Character.toString((char) code + 65);
    }
}
