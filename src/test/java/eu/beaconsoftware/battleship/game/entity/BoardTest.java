package eu.beaconsoftware.battleship.game.entity;

import eu.beaconsoftware.battleship.game.model.Board;
import eu.beaconsoftware.battleship.game.model.DefaultShipArrangementService;
import eu.beaconsoftware.battleship.game.model.ShipArrangementService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.IntStream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BoardTest {
    @ParameterizedTest
    @ValueSource(ints = {-2, 0, 1, 4, 27, Integer.MAX_VALUE})
    public void shouldThrowExceptionForWrongBoardSize(final int sizeUnderTest) {
        assertThatThrownBy(() -> {
            new Board(sizeUnderTest, new DefaultShipArrangementService());
        }).isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Size have to be between 5 and 26");
    }

    @Test
    public void shouldBuildCorrectBoardRepresentation() {
        final var board = new Board(10, new DefaultShipArrangementService());
        final var boardRepresentation = board.representation();

        assertThat(boardRepresentation.boardMap()).usingRecursiveComparison()
                .isEqualTo(
                        Collections.unmodifiableMap(new TreeMap<>() {
                            {
                                put("A", slots("A", 10));
                                put("B", slots("B", 10));
                                put("C", slots("C", 10));
                                put("D", slots("D", 10));
                                put("E", slots("E", 10));
                                put("F", slots("F", 10));
                                put("G", slots("G", 10));
                                put("H", slots("H", 10));
                                put("I", slots("I", 10));
                                put("J", slots("J", 10));
                            }
                        })
                );
    }

    private List<Slot> slots(final String column, final Integer rows) {
        return IntStream.range(1, rows + 1).mapToObj(row -> new Slot(column, row)).toList();
    }

    @Test
    public void shouldHitTheShipButNotSink() {
        // given
        final var arrangementService = mock(ShipArrangementService.class);
        final var ship = new Ship(ShipType.DESTROYER, ShipOrientation.VERTICAL);
        when(arrangementService.draw(any(), any())).thenReturn(List.of(
                new Slot("A", 1),
                new Slot("A", 2),
                new Slot("A", 3),
                new Slot("A", 4)
        ));
        final var board = new Board(10, arrangementService);
        board.place(ship);

        // when
        final var result = board.hit(new Slot("A", 1));

        // then
        assertThat(result).isEqualTo(HitResult.HIT);
        assertThat(ship.isSunk()).isNotEqualTo(true);
    }

    @Test
    public void shouldSinkTheShip() {
        // given
        final var arrangementService = mock(ShipArrangementService.class);
        final var ship = new Ship(ShipType.DESTROYER, ShipOrientation.VERTICAL);
        when(arrangementService.draw(any(), any())).thenReturn(List.of(
                new Slot("A", 1),
                new Slot("A", 2),
                new Slot("A", 3),
                new Slot("A", 4)
        ));
        final var board = new Board(10, arrangementService);
        board.place(ship);

        // when
        board.hit(new Slot("A", 1));
        board.hit(new Slot("A", 2));
        board.hit(new Slot("A", 3));
        board.hit(new Slot("A", 4));

        // then
        assertThat(ship.isSunk()).isTrue();
    }

    @Test
    public void shouldNotSinkTheShipWhenTheSameSlotHitMultipleTimes() {
        // given
        final var arrangementService = mock(ShipArrangementService.class);
        final var ship = new Ship(ShipType.DESTROYER, ShipOrientation.VERTICAL);
        when(arrangementService.draw(any(), any())).thenReturn(List.of(
                new Slot("A", 1),
                new Slot("A", 2),
                new Slot("A", 3),
                new Slot("A", 4)
        ));
        final var board = new Board(10, arrangementService);
        board.place(ship);

        // when
        board.hit(new Slot("A", 1));
        board.hit(new Slot("A", 2));
        board.hit(new Slot("A", 3));
        board.hit(new Slot("A", 1)); // <---- Again first slot

        // then
        assertThat(ship.isSunk()).isNotEqualTo(true);
    }

}