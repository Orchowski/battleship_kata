package eu.beaconsoftware.battleship.game.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class SlotTest {

    @ParameterizedTest(name = "{index} => location={0}, expected={1}")
    @DisplayName("Should parse location string to slot correctly")
    @MethodSource("slotTestParamsProvider")
    void slotLocationShouldBeParsedCorrectly(String location, Optional<Slot> expected) {
        assertThat(Slot.fromText(location))
                .usingRecursiveComparison().isEqualTo(expected);
    }

    private static Stream<Arguments> slotTestParamsProvider() {
        return Stream.of(
                Arguments.of("A10", Optional.of(new Slot("A", 10))),
                Arguments.of("A1", Optional.of(new Slot("A", 1))),
                Arguments.of("W199", Optional.of(new Slot("W", 199))),
                Arguments.of(" A10", Optional.empty()),
                Arguments.of("AB10", Optional.empty()),
                Arguments.of("A 1", Optional.empty()),
                Arguments.of(" ", Optional.empty())
        );
    }
}
