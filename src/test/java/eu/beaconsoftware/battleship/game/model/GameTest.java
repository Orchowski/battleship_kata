package eu.beaconsoftware.battleship.game.model;

import eu.beaconsoftware.battleship.game.entity.Slot;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GameTest {
    @Test
    public void shouldInitializeOnStart() {
        // given
        var game = new Game();

        // when
        var result = game.startNewGame();

        // then
        assertThat(result).isNotNull();
        assertThat(result.boardMap()).isNotNull();
        assertThat(result.boardMap().size()).isGreaterThan(0);
    }

    @Test
    public void shouldValidateLocation() {
        // given
        var game = new Game();
        game.startNewGame();

        // when
        var result = catchThrowable(() -> game.hit("2A"));

        // then
        assertThat(result)
                .isNotNull()
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("wrong location");
    }

    @Test
    public void shouldValidateIfSlotIsOnBoard() {
        // given
        var game = new Game();
        game.startNewGame();

        // when
        var result = catchThrowable(() -> game.hit("Z2"));

        // then
        assertThat(result)
                .isNotNull()
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("Out of board size");
    }

    @Test
    public void shouldValidateIfGameIsStarted() {
        // given
        var game = new Game();

        // when
        var result = catchThrowable(() -> game.hit("Z2"));

        // then
        assertThat(result)
                .isNotNull()
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("Game should be started");
    }

    @Test
    public void shouldSinkShips() {
        // given
        final var arrangementService = mock(ShipArrangementService.class);
        when(arrangementService.draw(any(), any())).thenReturn(List.of(
                new Slot("A", 1),
                new Slot("A", 2),
                new Slot("A", 3),
                new Slot("A", 4),
                new Slot("A", 5)
        ), List.of(
                new Slot("B", 1),
                new Slot("B", 2),
                new Slot("B", 3),
                new Slot("B", 4)
        ), List.of(
                new Slot("C", 1),
                new Slot("C", 2),
                new Slot("C", 3),
                new Slot("C", 4)
        ));

        var game = new Game(arrangementService);
        game.startNewGame();

        //when
        game.hit("A1");
        game.hit("A2");
        game.hit("A3");
        game.hit("A4");
        game.hit("A5");
        game.hit("B1");
        game.hit("B2");
        game.hit("B3");
        game.hit("B4");
        game.hit("C1");
        game.hit("C2");
        game.hit("C3");
        var result = game.hit("C4");

        //thenÓ
        assertThat(result.allShipsSunk()).isTrue();
    }
}