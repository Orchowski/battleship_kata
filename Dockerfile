FROM openjdk:17
COPY . /opt/project
WORKDIR /opt/project
RUN ["./gradlew", "build"]
CMD ["./gradlew", "bootRun"]
EXPOSE 8080