# BATTLESHIP GAME
## How to run?
### Requirements

- Possibility of running bash scripts
    - git bash/WSL on Windows
    - bash/sh/zsh etc. on Linux
    - bash/sh/zsh etc. on OSX
- Docker

### How to:
Enter to the main directory and run the `./run-docker.sh` script.
It builds a docker image and runs it. After stopping the container, it should remove automatically.

- Application is available on port 8080


## Used tools and why

- Java 17 - because the last version of java I used was 11, and I wanted to experiment a little
- SpringBoot & Thymeleaf - because It was the easiest way to implement some simple UI and you can run it fast without
setting up SDKs etc. Even simpler than a command line.
    - I considered also bundling it into WASM and running statically (JVM is not the best for such use)
    - Java QT Jambi - but the effort would be too high for me
    - Android app - because logic would be written in java, and UI would be pretty straightforward to implement too. 
I rejected this idea because I was not sure if you'll be able to run it quickly.  
- ~~Spock~~ for tests - Spock data-driven tests are great, and I wanted to use them,
but I had some problems setting it up (new gradle), and I just decided to use junit5 and stop wasting time.



## About the implementation
I decided to implement it simply. I know it's not OOP or even close, but I followed some good practices.
Cohesion is pretty good; responsibility is rather separated. The design allows changing this game relatively easy to introduce
player vs. player game - ShipArrangementService has to be implemented differently and added notion of Player.
Everything is quite testable.

Some conventions I'm using may be weird for you, but I'm open to discuss them :)  